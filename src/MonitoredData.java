import java.util.Date;

public class MonitoredData {
	private Date startTime;
	private Date endTime;
	private String acivity;
	
	
	public MonitoredData(Date startTime,Date endTime,String activity){
		this.startTime=startTime;
		this.endTime=endTime;
		this.acivity=activity;
		
	}
	


	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getAcivity() {
		return acivity;
	}
	public void setAcivity(String acivity) {
		this.acivity = acivity;
	}

}
