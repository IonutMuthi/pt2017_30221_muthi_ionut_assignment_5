import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class Main  {
	public static ArrayList<MonitoredData> monitoredData=new ArrayList();
	public static BufferedReader reader;
	public static void main(String[] args) throws IOException {

		citireStream();
		
	

		try {
		    PrintWriter writer = new PrintWriter("ActivitiesRezult.txt", "UTF-8");
		    writer.println("1) Number of distinct days is:");
		    writer.println(coundDays());
		    writer.println("");
		    writer.println("2) Map of each distinct action type and the number of occurrences in the log:");
		    writer.println(timesMap());
		    writer.println("");
		    writer.println("3) Activity count for each day of the log:");
		    writer.println(activityDay());
		    writer.println("");
		    writer.println("4)  Activities with total duration larger than 10 hours:");
		    writer.println(countTime());
		    writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	
		
		//coundDays();
		//timesMap();
		//activityDay();
		//countTime();
		
		
//		
//		Map<String,Long> duration=new HashMap();
//		Map<String,Long> lessThan5=new HashMap();
//		
//		duration=monitoredData.stream().collect(Collectors.groupingBy(e->e.getAcivity(),Collectors.summingLong(e->e.getEndTime().getTime()-e.getStartTime().getTime())));
//		
//		long comp=(long) (0.9*(5*60000));
//		ArrayList<String> acc=new ArrayList<>();
//		lessThan5= duration.entrySet().stream().filter(e->e.getValue()<comp).collect(Collectors.toMap(e->e.getKey(), e->e.getValue()));
//		System.out.println();
//		System.out.println();
//		System.out.println(lessThan5);
		
		
		
	}
	
	

			
		
	
	
	static Map<String,Long> countTime(){
		
	Map<String,Long> duration=new HashMap();
		
		duration=monitoredData.stream().collect(Collectors.groupingBy(e->e.getAcivity(),Collectors.summingLong(e->e.getEndTime().getTime()-e.getStartTime().getTime())));
		
		System.out.println(duration);
		

		Map<String,Long> durationFiltered=new HashMap();
		
		durationFiltered=duration.entrySet().stream().filter(e->e.getValue()>10*3600000).collect(Collectors.toMap(e->e.getKey(), e->e.getValue()));
		
		System.out.println(durationFiltered);
		
		return durationFiltered;
		

	}
	
	static Map<Integer, Map<String, Long>> activityDay(){
	Map<Integer, Map<String, Long>> activitiesDay=new HashMap();
		
		activitiesDay=monitoredData.stream().collect(Collectors.groupingBy(e->e.getStartTime().getDate(),Collectors.groupingBy(e->e.getAcivity(),Collectors.counting())));

		System.out.println(activitiesDay);
		System.out.println();
		return activitiesDay;
		
	}
	
	static Map<String, Long> timesMap(){
	Map<String, Long> activity=new HashMap<>();
		
		activity=monitoredData.stream().collect(Collectors.groupingBy(e->e.getAcivity(),Collectors.counting()));
		
		Set key=activity.keySet();
		Iterator interator=key.iterator();
		while(interator.hasNext()){
			String el=(String) interator.next();
			Long val=activity.get(el);
			System.out.println(el+" "+ val);
		}
		
		return activity;
		
	}
	
	
	@SuppressWarnings("deprecation")
	static long coundDays(){
		
		long days=monitoredData.stream().map(e->e.getStartTime().getDate()).distinct().count();
		System.out.println("There are "+days+" distinct days.");
		return days;
	
	}
	
	
	static void citireStream() throws IOException{
		 File file=new File("Activities.txt");
		  reader=new BufferedReader(new InputStreamReader(new FileInputStream(file)));
		
		SimpleDateFormat date=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		
		String citesc = null;
		try {
			citesc=reader.readLine();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		while(citesc!=null){
			
			Scanner str=new Scanner(citesc);
			String dataStart=str.next()+" "+str.next();
			String dataStop=str.next()+" "+str.next();
			String activity=str.next();
			try {
				MonitoredData mData=new MonitoredData(date.parse(dataStart),date.parse(dataStop),activity);	
				monitoredData.add(mData);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		citesc=reader.readLine(); 
		
		}
	}

}
